//          [MODELS]
const User = require('../models/User.js');
const Product = require('../models/Product.js');
const Order = require('../models/Order');

module.exports.addToCard = (reqParams) => {

    //access first the info from product DB
    let infoProduct =  
        Product.findById(reqParams.userId)
            .then((result, error) => {
                return error ?  error : result
            });

    //access the info from order DB
    let infoOrder =
        Order.findById(reqParams.userId)
            .then((result, error) => {
                return error ? error : result
            });

    // reconstruct the data and hold only
    const data = {
        userId : infoOrder.order_userId,
        orderedProducts : [{
            productId : infoOrder.order_products.order_productsId,
            productName : infoOrder.order_products.order_productsName,
            productDescription : infoProduct.product_description,
            // productPice : infoOrder.product_

        }]
    }

}