//          [MODELS]
const User = require('../models/User.js');
const Product = require('../models/Product.js');
const Order = require('../models/Order');

const bcrypt = require('bcrypt');   //password encrypt
const auth = require('../auth.js'); //auth


//          [CREATE A PRODUCT FOR ADMIN ONLY]
/*
STEPS:
1. let the user enter the  auth 
2. enter to the body the product details
3. check the user if the user is admin or not
4. if yes, create the desired product else not authorized
*/

module.exports.createProduct = async (reqBody, isAdmin) => {

    // //test if the product is already exist
    // let isProductExistAlready = await Product.find({product_name : reqBody.product_name})
    //     .then((result, error ) => {
    //         if(error) {
    //             return error
    //         } else if (result.length > 0)
    //             return true
    //         else {
    //             return false
    //         }
    //     }); 

    


    console.log('Admin: ' + isAdmin)
    // truthy - create the product
    if (isAdmin) {

        // //if false meaning there is no existing
        // if (!isProductExistAlready) {     

            //Create a product and hold only at the moment
            //not yet saved
            let newProduct = new Product({
                product_name : reqBody.product_name,
                product_description: reqBody.product_description,
                product_price : reqBody.product_price,
                product_quantity : reqBody.product_quantity
            });

            //saved the product
            return await newProduct.save().then((success, error) => {
                console.log("success: " + success)
                    if (success) {
                        return true;
                    }
                

            });

        // } else {
        //     return false
        // }
 
    } else {
        //return a promise here
        return false;
    }
}


//              [GET ALL PRODUCTS]
module.exports.getAllProducts = async (isAdmin) => {
    console.log('isAdmin ---' + isAdmin )

    //use '!' to catch the returned data as false to the front end
    //receiving as false, then use '!' to reverse to catch the 'false' 
    // isAdmin: false
    if(isAdmin) {

        //returned info to frontend is an object 'result'
        return Product.find({}).then((result, error) => {
            console.log("length: " + result.length)
            return result
            
        });

    //isAdmin === true
    } else {
        return false;
    }


}



//              [GET ALL ACTIVE PRODUCTS]
//STEPS:
//1. using the find method and product_isActive : true
module.exports.getAllActiveProducts = () => {
    return Product.find({product_isActive : true})
        .then((result,error) => {

            console.log("ALL ACTIVE PRODUCTS : " + result )
            if(!error){
                return result;
            } else {
                return false;
            }
        });
}


//              [GET A SINGLE PRODUCT]
//STEPS:
//1. get ID from req.params and use the findById from Product model 
module.exports.getAProduct = (reqParams) => {
    return Product.findById(reqParams.product_id)
        .then((result, error) => {
            if(result){
                return result;
            } else {
                return false;
            }
        });
}


//              [PUT/UPDATE AN ITEM (ADMIN)]
// steps:
// 1. get id first from params
// 2. get the isAdmin from auth
// 3. can update if isAdmin: true
// 4. Not authorized to update.

module.exports.updateProduct = (reqParams, isAdmin, reqBody) => {

    //if admin can edit
    if (isAdmin && reqBody !== 'undefined') {

        //store and hold the data from reqBody
       let toUpdateProduct = {
        product_name : reqBody.product_name,
        product_description: reqBody.product_description,
        product_price : reqBody.product_price,
        product_quantity : reqBody.product_quantity,
        product_isActive : reqBody.product_isActive
       };

       return Product.findByIdAndUpdate(reqParams.product_id, toUpdateProduct)
            .then((result, error) => {
                console.log("data ----" + result)
                if (result){
                    return result;
                } else {
                    return false;
                }
            })
        

    } else {
        return Promise.reject(false);
    }
}



//              [ARCHIVE A PRODUCT (ADMIN)]
module.exports.archiveProduct = (reqParams, isAdmin, reqBody) => {

    //isAdmin : true
    if (isAdmin && reqBody !== 'undefined') {

        //store and hold only , not yet saved the data
        //create a variable to store the data from reqBody
        let archiveProduct = {
            product_isActive : reqBody.product_isActive
        }

        //saved the data to DB
        return Product.findByIdAndUpdate(reqParams.product_id, archiveProduct)
            .then((result, error) => {
                if (!error){
                    return true;
                } else {
                    return false;
                }
            })

    } else {
        return Promise.reject('Failed: Not authorized to archive the product or request body is missing.');
    }
}