//      [MONGODB]
const mongoose = require('mongoose');


//      [SCHEMA]
const cartSchema = new mongoose.Schema({

    addToCart_userId : {type:Number},

    //all details of ordered products
    addToCart_orderedProducts: [{
        addToCart_productId : {type: String},
        addToCart_productName : {type: String},
        addToCart_productDescription : {type : String},
        addToCart_productPrice : {type: Number},
        addToCart_productQuantity : {type: Number},
        addToCart_productTotalPrice : {type: Number},
        addToCart_createdOn : {type: Date, default: new Date()}

    }],
    addToCart_totalItemsInCart : {type: Number},
    addToCart_grandTotalPrice : {type: Number},
    addToCart_status : {type: String, default: "pending"}
})

module.exports= mongoose.model('Cart', cartSchema);