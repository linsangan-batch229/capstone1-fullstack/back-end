//      [MONGODB]
const mongoose = require('mongoose');

//      [ORDERS SCHEMA]
const orderSchema = new mongoose.Schema({
    //ObjectId
    order_userId : {
        type : String,
    },

    //Array of objects
    order_products : [{
        order_productId : {type : String},
        order_productName : {type : String},
        order_productDescription : {type: String},
        order_productPrice : {type: Number},
        order_productQuantity : {type: Number},
        order_productTotalPrice : {type: Number},
        order_createdOn : {type: Date, default : new Date()}
    }],

    ///total amount
    order_grandTotalPrice : {type: Number, default : 0}
})

module.exports = mongoose.model('Order', orderSchema);