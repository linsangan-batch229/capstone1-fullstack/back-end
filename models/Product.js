const mongoose = require('mongoose');

//          [PRODUCT SCHEMA]
const productSchema = new mongoose.Schema({
    product_name : {
        type: String,
        require : [true, "Product name is required"]
    },

    product_description : {
        type : String,
        require : [true, "Product description is required"]
    },

    product_price : {
        type: Number, 
        require : [true, "Product price is required"]
    },

    product_quantity : {
        type: Number,
        require : [true, "Product quantity is required"]
    },

    product_isActive : {
        type: Boolean,
        default : true
    },

    product_createdOn : {
        type : Date,
        default : new Date()
    }

})


//          [PRODUCT MODEL]
module.exports = mongoose.model("Product", productSchema);