//          [ROUTES]
const userRoutes = require('./user.js');
const productRoutes = require('./product.js');
const orderRoutes = require('./order.js');


//          [MODELS]
const User = require('../models/User.js');
const Product = require('../models/Product.js');
const Order = require('../models/Order');

//          [CONTROLLER]
const addToCardController = require('../controllers/addToCartController.js');

//          []
const express = require('express');
const auth = require("../auth.js");
const router = express.Router();


// //          [ADD TO CARD ]
router.post('/:userID', (req, res) => {
    addToCardController.addToCard(req.params)
        .then(result => res.send(result))
        .catch(error => res.send(error));
})





//          [EXPORT 'router' ]
module.exports = router;