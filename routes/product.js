const express = require('express');
const router = express.Router();
const productController = require('../controllers/productController');
const auth = require("../auth.js");

//          [CREATE PRODUCT FOR ADMIN ONLY]
router.post('/createProduct', auth.verify, (req, res) => {

    const isAdmin = auth.decode(req.headers.authorization).isAdmin;

    productController.createProduct(req.body, isAdmin)
        .then(result => res.send(result))   //success
        .catch(error => res.send(error));   //catch the Promise.reject
})


//          [GET ALL PRODUCTS]
router.get('/getProductAll', auth.verify, (req, res) => {

    const isAdmin = auth.decode(req.headers.authorization).isAdmin;

    productController.getAllProducts(isAdmin)
        .then(result => res.send(result))
        .catch(error => res.send(error));

})



//          [GET ALL ACTIVE PRODUCTS]
router.get('/getProductActive', (req, res) => {
    productController.getAllActiveProducts()
        .then(result => res.send(result))
        .catch(error => res.send(error)); 

})


//          [GET A SINGLE PRODUCTS]
router.get('/getProduct/:product_id', (req, res) => {
    productController.getAProduct(req.params)
        .then(result => res.send(result))
        .catch(error => res.send(error)); 
})



//          [PUT/UPDATE AN ITEM (ADMIN)]
// steps:
// 1. get id first from params
// 2. check if the id is na admin
// 3. 
router.put('/updateProductInfo/:product_id', auth.verify, (req, res) => {

    const isAdmin = auth.decode(req.headers.authorization).isAdmin;

    productController.updateProduct(req.params, isAdmin, req.body)
        .then(result => res.send(result))
        .catch(error => res.send(error));
})



//          [ARCHIVE A PRODUCT]
router.put('/archive/:product_id',  auth.verify, (req, res) => {

    const isAdmin = auth.decode(req.headers.authorization).isAdmin;

    productController.archiveProduct(req.params, isAdmin, req.body)
        .then(result => res.send(result))   //if success
        .catch(error => res.send(error))    //catch the error of Promise.reject
})


//          [FIND INFO ABOUT]




//          [EXPORT 'router' ]
module.exports = router;